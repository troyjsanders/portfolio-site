import React from "react";

function Html() {
  return (
    <>
      <h1
        style={{
          position: "absolute",
          top: "25vh",
          left: "50vw",
          transform: "translateX(-50%)",
          color: "#f9f4f8",
        }}
      >
        Troy Sanders
      </h1>
      <h2
        style={{
          position: "absolute",
          top: "55vh",
          left: "50vw",
          transform: "translateX(-50%)",
          color: "#f9f458",
        }}
      >
        Immersive Technologist & Innovation Developer
      </h2>

      <h2
        style={{
          position: "absolute",
          top: "140vh",
          left: "50vw",
          transform: "translateX(-65%)",
          color: "#f4b677",
        }}
      >
        Charles Schwab
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "155vh",
          left: "50vw",
          transform: "translateX(-90%)",
          color: "#f4b677",
        }}
      >
        2015-Present
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "165vh",
          left: "50vw",
          transform: "translateX(-75%)",
          color: "#f4b637",
        }}
      >
        Technical Lead of the Advisor Mobile application focusing on
        microservices / APIs.
      </h3>

      <h2
        style={{
          position: "absolute",
          top: "220vh",
          left: "50vw",
          transform: "translateX(-50%)",
          color: "#f4b677",
        }}
      >
        Wave
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "229vh",
          left: "50vw",
          transform: "translateX(-65%)",
          color: "#f4b677",
        }}
      >
        2016-2019
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "239vh",
          left: "50vw",
          transform: "translateX(-75%)",
          color: "#f4b637",
        }}
      >
        Designed a social, visual, music festival platform in Virtual Reality
        focused on user generated content and tools for artists and performers.
      </h3>

      <h2
        style={{
          position: "absolute",
          top: "330vh",
          left: "50vw",
          transform: "translateX(-35%)",
          color: "#f4b677",
        }}
      >
        Viewer Ready
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "345vh",
          left: "50vw",
          transform: "translateX(-60%)",
          color: "#f4b677",
        }}
      >
        2017-2018
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "355vh",
          left: "50vw",
          transform: "translateX(-75%)",
          color: "#f4b637",
        }}
      >
        Architected multiplayer game modes for two Virtual Reality games:
        Pitch-hit and Super Hoops.
      </h3>

      <h2
        style={{
          position: "absolute",
          top: "420vh",
          left: "50vw",
          transform: "translateX(-65%)",
          color: "#f4b677",
        }}
      >
        Battlerock Studios
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "435vh",
          left: "50vw",
          transform: "translateX(-115%)",
          color: "#f4b677",
        }}
      >
        2013-Present
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "445vh",
          left: "50vw",
          transform: "translateX(-92%)",
          color: "#f4b637",
        }}
      >
        Technical Director of various mobile, AR/VR, and web projects.
      </h3>

      <h2
        style={{
          position: "absolute",
          top: "500vh",
          left: "50vw",
          transform: "translateX(-45%)",
          color: "#f4b677",
        }}
      >
        New Resources Consulting
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "522vh",
          left: "50vw",
          transform: "translateX(5%)",
          color: "#f4b677",
        }}
      >
        2013-Present
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "532vh",
          left: "50vw",
          transform: "translateX(-75%)",
          color: "#f4b637",
        }}
      >
        NRC BUILD team, responsible for maintaining and supporting 20+
        client-owned web and mobile applications.
      </h3>

      <h2
        style={{
          position: "absolute",
          top: "610vh",
          left: "50vw",
          transform: "translateX(-45%)",
          color: "#f4b677",
        }}
      >
        QBE Insurance
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "625vh",
          left: "50vw",
          transform: "translateX(5%)",
          color: "#f4b677",
        }}
      >
        2012-2014
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "635vh",
          left: "50vw",
          transform: "translateX(-75%)",
          color: "#f4b637",
        }}
      >
        Internal .NET C# application for tracking inventory.
      </h3>

      <h2
        style={{
          position: "absolute",
          top: "695vh",
          left: "50vw",
          transform: "translateX(-45%)",
          color: "#f4b677",
        }}
      >
        608 Games
      </h2>
      <h3
        style={{
          position: "absolute",
          top: "710vh",
          left: "50vw",
          transform: "translateX(5%)",
          color: "#f4b677",
        }}
      >
        2012-2012
      </h3>
      <h3
        style={{
          position: "absolute",
          top: "720vh",
          left: "50vw",
          transform: "translateX(-75%)",
          color: "#f4b637",
        }}
      >
        Technical Director of Educational Games.
      </h3>
    </>
  );
}

export { Html as default };
