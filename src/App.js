import { Suspense, useEffect } from "react";
import { Canvas, useThree } from "@react-three/fiber";
import { Scene } from "./Scene";
import styles from "./styles.css";

function AdaptivePixelRatio() {
  const current = useThree((state) => state.performance.current);
  const setPixelRatio = useThree((state) => state.setDpr);
  useEffect(() => {
    setPixelRatio(window.devicePixelRatio * current);
  }, [current, setPixelRatio]);
  return null;
}

export default function App() {
  return (
    <Canvas
      dpr={[1, 2]}
      gl={{ antialias: false, powerPreference: "low-power" }}

    >
      <Suspense fallback={'Loading...'}>
        <ambientLight />
        <directionalLight color="red" intensity={10} />
        <AdaptivePixelRatio />

        <Scene />
      </Suspense>
    </Canvas>
  );
}
